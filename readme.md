# Adobe Experience Manager
Adobe Experience Manager (AEM) è una soluzione completa per la gestione dei contenuti per la creazione di siti web, app mobili e moduli
Consulta queste guide, tutorial video e altre risorse di apprendimento per implementare e utilizzare AEM

## Maggiori informazioni
Adobe Experience Manager è un sistema client-server basato sul Web per la creazione, gestione e distribuzione di siti Web commerciali e di servizi correlati, che combina varie funzioni a livello di infrastruttura e di applicazione in un unico pacchetto integrato.

AEM offre anche una serie di funzionalità a livello di applicazione per la gestione di:

- Siti Web

- Applicazioni mobili

- Pubblicazioni digitali

- Forms

- Risorse digitali

- Community

- Commerce online


I clienti possono utilizzare questi blocchi di base a livello di infrastruttura e applicazione per creare soluzioni personalizzate creando applicazioni proprie.

Il server AEM è Java-based.

AEM è disponibile e viene offerto ai clienti in 2 tipi di soluzioni:

- In sede: AEM implementato e gestito nell'ambiente aziendale.

- Managed Services - Cloud Manager per Adobe Experience Manager: AEM distribuito e gestito da Adobe Managed Services.


Maggiori dettagli [qui](https://experienceleague.adobe.com/docs/experience-manager-64/deploying/deploying/deploy.html?lang=it-IT#basic-concepts)

Per la storia di come è nato ecco un [link](http://www.aemcq5tutorials.com/tutorials/adobe-aem-history/)


## Chi lavora con AEM?

Per quanto riguarda SITES (modulo AEM per la creazione e gestione dei siti/portali) abbiamo prevalentemente 2 profili:

- SVILUPPATORE

- UTENTE/CONTENT FILLER

Per il profilo SVILUPPATORE, abbiamo 3 tipi di profili con determinate skills:

- Sviluppatore JAVA

- Sviluppatore Front-End

- Sviluppatore FULL-STACK

Per il profilo utente/content filler si richiede conoscenza nell'utilizzo di CMS in genere (Wordpress, Joomla, Liferay, ecc.).

## Progetti importanti in AEM

Alcuni gestiti e/o realizzati da HRM:

- [PRADA](https://www.prada.com)

- [STELLANTIS](https://www.stellantis.com)

- [ESSELUNGA](https://www.esselunga.it)

- [OASITIGRE](https://www.oasitigre.it)

- [SISAL](https://www.sisal.it)

- [BOSE](https://bose.it)

- [CONAD](https://www.conad.it)

- [TRENITALIA](https://www.trenitalia.com)

- [VOLKSWAGEN](https://www.volkswagen.it)

- [COSTACROCIERE](https://www.costacrociere.it)


## Per avvicinarsi a Experience Manager
Per la parte SITES come developer devi conoscere:

- JAVA (preferibile J2EE e librerie rivolte al WEB) e/o HTML, CSS, JAVASCRIPT, JQUERY.

- RESTful Web Service.

- CMS in generale, cos'è e come funziona.

- Sling Web Application, JCR (Java Content Repository), Maven

## Dove trovare materiali utili

- [Guida ufficiale Adobe](https://helpx.adobe.com/experience-manager/tutorials.html)

- [Come predisporre l'ambiente per iniziare a lavorare su AEM](https://experienceleague.adobe.com/docs/experience-manager-64/deploying/deploying/deploy.html?lang=it-IT#getting-started)

- [Quali sono gli esercizi base fondamentali per avere chiaro il contesto?](https://experienceleague.adobe.com/docs/experience-manager-learn/getting-started-wknd-tutorial-develop/overview.html?lang=it)


## Video training:

#### What is AEM
https://www.youtube.com/watch?v=zA53zv0l-9I

#### Architecture of AEM
https://www.youtube.com/watch?v=AERbIntqfTk

#### AEM Installation
https://www.youtube.com/watch?v=OhTA69mzDg0&t=498s

#### Start AEM Graphically and by Command prompt
https://www.youtube.com/watch?v=XM4IM_G7MTo

#### Authoring Basics
https://www.youtube.com/watch?v=Q7IfWcVYqxg

#### AEM developer tools
https://www.youtube.com/watch?v=sajypJ9q8Ws

#### How to install Package
https://www.youtube.com/watch?v=Zq5YO2XlWdA

#### Folder structure of repository
https://www.youtube.com/watch?v=OxwSXQUYNfg

#### Create Page rendering component in AEM
https://www.youtube.com/watch?v=JjduMlTa2vo

#### Create a template in AEM
https://www.youtube.com/watch?v=g0ldLTNfj3o

#### Apache Sling resource resolution with example
https://www.youtube.com/watch?v=Q0qJHzvKj4Y

#### Sightly / HTL language
https://www.youtube.com/watch?v=lov6tA8b_pE

#### Sightly Syntax with examples
https://www.youtube.com/watch?v=FRHq1gIAVxs
 
#### CurrentPage, properties and currentNode object in AEM
https://www.youtube.com/watch?v=oTP6xfu6Uig

#### Client library in AEM
https://www.youtube.com/watch?v=8i2Uy_99_TU